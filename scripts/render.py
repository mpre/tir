from graphviz import Digraph
import sys

name = sys.argv[1]
outputdir = sys.argv[2]

dot = Digraph(comment=name, engine='neato')
dot.body.extend(['rankdir=LR'])
fin = open(sys.argv[1], 'r')

num_elements=0

for l in fin.readlines():
    num_elements += 1
    l = l.split()
    subg = Digraph("cluster_{0}".format(l[0]))
    subg.body.append('style=filled')
    ## EDIT FOR NEATO
    # if(int(l[3]) > 62):
    #     subg.body.append('color=lightblue')
    # else:
    #     subg.body.append('color=lightgrey')
    if(int(l[3]) > 62):
        c = 'lightblue'
    else:
        c = 'white'
    subg.body.append("label = \"unitig {0}\"".format(l[0]))
    subg.node("{0}{1}".format(l[0], 'R'), "{0}{1}({2})".format(l[0], 'R', l[3]), fillcolor='red' if l[2] == '1' else c, style='filled')
    subg.node("{0}{1}".format(l[0], 'L'), "{0}{1}({2})".format(l[0], 'L', l[3]), fillcolor='red' if l[1] == '1' else c, style='filled')
    subg.edge("{0}{1}".format(l[0], 'L'), "{0}{1}".format(l[0], 'R'), dir='both', color='red', len='1.5')
    # subg.edge("{0}{1}".format(l[0], '+'), "{0}{1}".format(l[0], '-'))
    dot.subgraph(subg)
fin.close()

fin = open(sys.argv[1], 'r')
for l in fin.readlines():
    l = l.split()
    for i in range(4, len(l))[::3]:
        dot.edge("{0}{1}".format(l[0], l[i]), "{0}{1}".format(l[i+1], l[i+2]), dir='both', len='2.0')

dot.render('{0}/{2}/{1}.gv'.format(outputdir, name.replace("ccs/", ""), num_elements))
