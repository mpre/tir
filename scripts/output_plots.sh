DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p graphs
mkdir -p graphs/$1
for name in ccs/cc.*.plain.txt; do
    size_of_cc=$(wc -l < "$name")
    if [ $size_of_cc == $1 ]; then
        echo $name
        python $DIR/render.py $name graphs
    fi
done
