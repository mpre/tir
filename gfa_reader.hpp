#ifndef GFA_READER_HPP
#define GFA_READER_HPP

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <limits>
#include <ios>
#include <chrono>
#include <ratio>
#include <ctime>
#include <cstdio>
#include <map>
#include <zlib.h>
#include <cmph.h>

#include "kseq.h"

#include "graph.hpp"
#include "util.hpp"

namespace GFA {
  using namespace std;

  bool read_structure_gfa(const string &gfafilename,
                          const unsigned int &ksize,
                          const unsigned int &maxK,
                          Graph &g); 

  bool read_structure_gfa_and_fasta(const string &gfafilename,
                                    const string &fastafilename,
                                    const unsigned int &ksize,
                                    const unsigned int &maxK,
                                    Graph &g,
                                    bool debug = false);
}

#endif
