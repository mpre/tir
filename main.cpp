#include <iostream>
#include <sstream>
#include <vector>
#include <limits>
#include <getopt.h>
#include <map>

#include <boost/filesystem.hpp>

#include "graph.hpp"
#include "gfa_reader.hpp"
#include "util.hpp"
#include "tir_algo.hpp"

#define PROGRAMNAME "tir"

using namespace std;
using namespace std::chrono;

namespace opt {
  static unsigned int ksize;
  static unsigned int maxk        = numeric_limits<unsigned int>::max();
  static bool         double_link = false;
  static bool         verbose     = false;
  static bool         debug       = false;
  static bool         outccs      = false;
  static string       gfafile     = "";
  static string       fastafile;
};

static const char *shortopts = "k:m:f:dvhDC";

static const struct option longopts[] = {
  {"fasta-file", required_argument,  NULL,  'f'},
  {"k-size",     required_argument,  NULL,  'k'},
  {"max-k",      required_argument,  NULL,  'm'},
  {"double-link",no_argument,        NULL,  'd'},
  {"verbose",    no_argument,        NULL,  'v'},
  {"debug",      no_argument,        NULL,  'D'},
  {"output-ccs", no_argument,        NULL,  'C'},
  {"help",       no_argument,        NULL,  'h'},
  {NULL, 0, NULL, 0}
};

static const char *USAGE_MESSAGE =
  "Usage: " PROGRAMNAME " [OPTION] ...  GFAFILE                            \n"
  "Removes tips from unitigs                                               \n"
  "\n"
  "OPTIONS\n"
  "  -k, --k-size=K               size of kmers in dBG (required)          \n"
  "  -m, --max-k=M                maximum length of tip (required)         \n"
  "  -f, --fasta-file=FASTAFILE   fasta input file (optional)              \n"
  "  -d, --double-link            output both links between two segments (default false)\n"
  "\n"
  "  -h, --help                   print this help and exit                 \n"
  "  -v, --verbose                verbose output                           \n"
  "\n"
  "[DEBUG OPTIONS]\n"
  "  -C, --output-ccs             output ccs in directory ccs              \n"
  "  -D, --debug                  show debug messages                      \n"
;

bool parse_options(int argc, char** argv) {
  bool parsepass = true, has_k = false, has_m = false;
  for(char c; (c = getopt_long(argc, argv, shortopts, longopts, NULL)) != -1; ) {
    istringstream arg(optarg != NULL ? optarg : "");
    switch(c) {
    case 'f': arg >> opt::fastafile; break;
    case 'k': arg >> opt::ksize; has_k = true; break;
    case 'm': arg >> opt::maxk;  has_m = true; break;
    case 'd': opt::double_link = true; break;
    case 'D': opt::debug = true;     break;
    case 'C': opt::outccs= true;     break;
    case 'v': opt::verbose = true;   break;
    case 'h': cerr << USAGE_MESSAGE; return false;
    case '?': parsepass = false;     break;
    }
  }
  if(!has_k or !has_m) {
    cerr << PROGRAMNAME ": -k and -m parameters required\n";
    parsepass = false;
  }
  else if(argc - optind < 1) {
    cerr << PROGRAMNAME ": missing arguments\n";
    parsepass = false;
  }
  else if(argc - optind > 1) {
    cerr << PROGRAMNAME ": too many arguments\n";
  }
  if(!parsepass) {
    cerr << USAGE_MESSAGE;
    return false;
  }
  opt::gfafile = argv[optind++];
  return true;
}

int main(int argc, char** argv) {

  if(!parse_options(argc, argv))
    return 1;

  cerr.precision(3);
  boost::filesystem::create_directory(boost::filesystem::path(wk_dir));
  PRINT_NOW("Working directory: " << wk_dir << '\n');

  Graph g;

  if(opt::fastafile == "") {
    if(! GFA::read_structure_gfa(opt::gfafile, opt::ksize, opt::maxk, g) ) {
      PRINT_NOW("[ERROR] Something went wrong. Aborting...\n");
      if(!opt::debug)
        boost::filesystem::remove_all(boost::filesystem::path(wk_dir));
      return 1;
    }
  } else if(! GFA::read_structure_gfa_and_fasta(opt::gfafile, opt::fastafile, opt::ksize, opt::maxk, g, opt::debug) ) {
    PRINT_NOW("[ERROR] Something went wrong. Aborting...\n");
    if(!opt::debug)
      boost::filesystem::remove_all(boost::filesystem::path(wk_dir));
    return 1;
  }

  PRINT_NOW("Computing ccs\n");
  vector<uint32_t> c = compute_cc(g);

  if(opt::verbose) {
    PRINT_NOW("Computing number of ccs\n");
    vector<bool> already_seen(c.size(), false);
    size_t ccnum = 0;
    for(size_t i = 0; i < c.size(); ++i) {
      if(i == Node::NO_NODE) continue;
      if(!already_seen[c[i]]) {
        already_seen[c[i]] = true;
        ++ccnum;
      }
    }
    PRINT_NOW("Number of ccs " << ccnum << endl);

    PRINT_NOW("Mean size of cc: " << (float)(g.size() - 1)/ccnum << endl);
  }

  PRINT_NOW("Partitioning ccs\n");
  vector<vector<uint32_t>> ccs(c.size());
  for(size_t i = 0; i < c.size(); ++i) {
    if(i == Node::NO_NODE) continue;
    ccs[c[i]].push_back(i);
  }

  if(opt::verbose) {
    size_t maxnum = 0;
    for(auto &x : ccs) {
      if(x.size() > maxnum) maxnum = x.size();
    }
    PRINT_NOW("Max size of cc: " << maxnum << '\n');
  }

  if(opt::outccs) {
    PRINT_NOW("[!!!] Output ccs (this is ONLY for debug purposes and requires some time)\n");
    output_ccs(g, c, ccs);
  }

  PRINT_NOW("Removing tips\n");
  //#pragma omp for schedule(static, 50)
  for(size_t i = 0; i < ccs.size(); ++i) {
    if(ccs[i].size() < cc_max_size)
      remove_tips(ccs[i], g, opt::ksize, opt::maxk);
  }

  g.clear();

  PRINT_NOW("Performing merge and remove operations\n");
  map<size_t, pair<bool, size_t> > rename_nodes =
    merge_and_remove(ccs, opt::fastafile, c.size(), opt::ksize - 1);

  // for(auto const &x : rename_nodes) {
  //   cout << x.first << " " << x.second.first << " " << x.second.second << endl;
  // }
  // return 0;

  PRINT_NOW("Merging the tipped unitigs\n");
  merge_l_to_l(rename_nodes, opt::ksize - 1, opt::double_link);

  if(!opt::debug)
    boost::filesystem::remove_all(boost::filesystem::path(wk_dir));

  PRINT_NOW(PROGRAMNAME << " ended\n");

  return 0;
}

