#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <chrono>
#include <map>
#include <cmph.h>

#include "graph.hpp"

#define PRINT_NOW(S) {                                                  \
    cerr << fixed << setw(7) << CLOCK::NOW() << ": " << S;              \
  }

using namespace std;
using namespace std::chrono;

struct CLOCK {
  static const high_resolution_clock::time_point begin_t;
  static double NOW();
};

static const char COMPLEMENTS[128] = {
// 0   1   2   3   4   5   6   7   8   9
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  0
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  1
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  2
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  3
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  4
   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  5
   0,  0,  0,  0,  0, 84,  0, 71,  0,  0,  //  6
   0, 67,  0,  0,  0,  0,  0,  0,  0,  0,  //  7
   0,  0,  0,  0, 65,  0,  0,  0,  0,  0,  //  8
   0,  0,  0,  0,  0,  0,  0,116,  0,103,  //  9
   0,  0,  0, 99,  0,  0,  0,  0,  0,  0,  // 10
   0,  0,  0,  0,  0,  0, 97,  0,  0,  0,  // 11
   0,  0,  0,  0,  0,  0,  0,  0           // 12
};

static const string wk_dir = "___tir__internal__files/";
static const string hashfilename = wk_dir + "mphf.fn.cmph";
static const string deletedhashfilename = wk_dir + "mphf.fn.del.cmph";
// static const string alivehashfilename = "hash.lvl2.fn.cmph";
static const string wk_files_prefix = wk_dir + "cc.";
static const string seqs_file_suffix = ".seqs.fa";
static const string merge_ops_file_suffix = ".merge.ops";
static const string graph_file_suffix = ".plain.txt";
static const string deadseqfilename = wk_dir + "dead.seq.txt";
static const string aliveseqfilename = wk_dir + "alive.seq.txt";
static const string ltolfilename = wk_dir + "ltol.edges.txt";
static const string deadkckmfilename = wk_dir + "dead.kc.km.txt";
static const string alivekckmfilename = wk_dir + "alive.kc.km.txt";
static const string intraccs_edges_filename = wk_dir + "intraccs.edges.txt";
static const string keysfilename = wk_dir + "all.keys.txt.tmp";
static const string alivekeysfilename = wk_dir + "alive.keys.txt.tmp";

static const size_t max_partition_num = 200;

static const size_t cc_max_size = numeric_limits<size_t>::max();

cmph_t* build_mphf_em(const string &filename);
cmph_t* load_mphf(const string &filename);

char complement_side(const char &s);
bool bool_rc(const char &d);
char char_rc(const bool &d);

void output_ccs(Graph &g, const vector<uint32_t> &c, vector<vector<uint32_t> > &ccs);

#endif
