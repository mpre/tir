INCLUDEPATH=-I. -I${HOME}/local/include

CXX      = c++
# CXXFLAGS = -O0 -std=c++11 -Wall -I. -g
CXXFLAGSD = -O0 -std=c++11 -Wall ${INCLUDEPATH} -g -fopenmp
CXXFLAGS = -O9 -std=c++11 -Wall ${INCLUDEPATH} -DNDEBUG -fopenmp
CCLIB    = -L${HOME}/local/lib -lz -lcmph -lboost_system -lboost_filesystem -lemon
CCLIBD   = $(CCLIB)
BINNAME  = tir

all: tir

clean:
	rm -rf obj/*.o obj/*.o-debug $(BINNAME) $(BINNAME)-debug

obj/%.cpp.o: %.cpp
	@mkdir -p obj
	$(CXX) $(CXXFLAGS) -c $< -o $@	

obj/%.cpp.o-debug: %.cpp
	@mkdir -p obj
	$(CXX) $(CXXFLAGSD) -c $< -o $@

tir: obj/main.cpp.o obj/gfa_reader.cpp.o obj/graph.cpp.o obj/util.cpp.o obj/tir_algo.cpp.o
	$(CXX) $(CXXFLAGS) $^ -o $(BINNAME) $(CCLIB)

debug: obj/main.cpp.o-debug obj/gfa_reader.cpp.o-debug obj/graph.cpp.o-debug obj/util.cpp.o-debug obj/tir_algo.cpp.o-debug
	$(CXX) $(CXXFLAGSD) $^ -o $(BINNAME)-debug $(CCLIBD)

.PHONY: all tir debug clean
