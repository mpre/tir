#include "util.hpp"

const high_resolution_clock::time_point CLOCK::begin_t = high_resolution_clock::now();

double CLOCK::NOW() {
  auto now_t = high_resolution_clock::now();
  return duration_cast<duration<double>>(now_t - CLOCK::begin_t).count();
}

cmph_t* build_mphf_em(const string &filename) {
  FILE* keys_fd = fopen(filename.c_str(), "r");
  cmph_t *hash = NULL;
  if(keys_fd == NULL) {
    cerr << "Could not open file " << filename << "... aborting." << endl;
    return NULL;
  }
  cmph_io_adapter_t *source = cmph_io_nlfile_adapter(keys_fd);
  cmph_config_t *config = cmph_config_new(source);
  cmph_config_set_algo(config, CMPH_BDZ);
  hash = cmph_new(config);
  cmph_config_destroy(config);
  cmph_io_nlfile_adapter_destroy(source);
  fclose(keys_fd);
  return hash;
}

cmph_t* load_mphf(const string &filename) {
  FILE* mphfile = fopen(filename.c_str(), "rb");
  cmph_t *hashf = NULL;
  if(mphfile == NULL)  {
    cerr << "Could not open file " << filename << "... aborting." << endl;
    return NULL;
  }
  hashf = cmph_load(mphfile);
  fclose(mphfile);
  return hashf;
}

char complement_side(const char &s) {
  if(s == '+') return '-';
  return '+';
}

bool bool_rc(const char &d) {
  return d == '+' ;
}

char char_rc(const bool &d) {
  if(d) return '+';
  else return '-';
}

void output_ccs(Graph &g, const vector<uint32_t> &c, vector<vector<uint32_t> > &ccs) {
  for(size_t i = 0; i < c.size(); ++i) {
    if(ccs[c[i]].size() <= 1 || ccs[c[i]].size() > cc_max_size) continue;
    ostringstream filename;
    size_t cc_group = c[i] % max_partition_num;
    filename << wk_files_prefix << cc_group << graph_file_suffix;
    ofstream fout(filename.str(), ios::app);
    fout << i << "\t" << !g[i].can_merge_left() << "\t" << !g[i].can_merge_right() << "\t" << g[i].length();
    for(size_t j = 0; j < g[i].edge_num(); ++j) {
      nodeend end, other_end;
      uint32_t id;
      tie(end, id, other_end) = g[i].get_edge(j);
      // find other edge
      if(i < id) {
        fout <<"\t" << end_to_char(end) << "\t" << id << "\t" << end_to_char(other_end);
      }
    }
    fout << '\n';
  }
}
