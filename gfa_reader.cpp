#include "gfa_reader.hpp"

KSEQ_INIT(gzFile, gzread)

bool GFA::read_structure_gfa(const string &gfafilename,
                             const unsigned int &ksize,
                             const unsigned int &maxk,
                             Graph &g) {
  // TODO: improve this.  There are parts of the code copy-pasted from
  //       read_structure_gfa_and_fasta and probably we can read the gfa
  //       less times
  PRINT_NOW("GFA reads begin\n");
  int node_num = 1;

  // Read sequences tags and create a MPHF for them
  PRINT_NOW(" ↳ Creating tags file\n");
  {
    char record_type;
    ifstream input(gfafilename.c_str());
    ofstream tagsfile(keysfilename.c_str());
    while(input >> record_type) {
      switch(record_type) {
      case 'S': {
        string tag;
        string seq;
        input >> tag >> seq;
        tagsfile << tag << '\n';
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        ++node_num;
      }
        break;
      default:
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        break;
      }
    }
  }

  PRINT_NOW(" ↳ Computing MPHF of tags\n");
  cmph_t *hash = build_mphf_em(keysfilename);
  if(hash == NULL) {
    PRINT_NOW("  ↳ Error while building MPHF of keys in file " << keysfilename << "... aborting\n");
    return false;
  }

  vector<nodetype> nodes(node_num, nodetype::D);
  vector<tiplen_t> lengths(node_num);
  g.resize(node_num);

  PRINT_NOW(" ↳ Computing nodetypes\n");
  {
    char record_type;
    ifstream input(gfafilename.c_str());
    size_t id;
    while(input >> record_type) {
      switch(record_type) {
      case 'S': {
        string tag, seq;
        input >> tag >> seq;
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        id = cmph_search(hash, tag.c_str(), tag.size()) + 1;
        lengths[id] = seq.size() <= numeric_limits<tiplen_t>::max() ? seq.size() : numeric_limits<tiplen_t>::max();
        if(seq.size() <= maxk)
          nodes[id] = nodetype::S;
      }
        break;
      default:
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        break;
      }
    }
  }

  // mark long nodes next to short nodes
  {
    char record_type;
    ifstream input(gfafilename.c_str());
    while(input >> record_type) {
      switch(record_type) {
      case 'L': {
        string tagu, tagv;
        size_t idu, idv;
        char u_or, v_or;
        input >> tagu >> u_or >> tagv >> v_or;
        idu = cmph_search(hash, tagu.c_str(), tagu.size()) + 1;
        idv = cmph_search(hash, tagv.c_str(), tagv.size()) + 1;
        if(nodes[idu] == nodetype::S || nodes[idv] == nodetype::S) {
          nodes[idu] = min(nodes[idu], nodetype::LNS);
          nodes[idv] = min(nodes[idv], nodetype::LNS);
        }
        input.ignore(numeric_limits<streamsize>::max(), '\n');
      }
        break;
      default:
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        break;
      }
    }
  }

  PRINT_NOW(" ↳ Saving \"dead\" sequences\n");
  size_t alive_nodes = 0;
  {
    char record_type;
    string tag, seq;
    ifstream input(gfafilename.c_str());
    ofstream dead_seqs(deadseqfilename.c_str());
    ofstream alive_seqs(aliveseqfilename.c_str());
    size_t id;
    while(input >> record_type) {
      switch(record_type) {
      case 'S': {
        input >> tag >> seq;
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        id = cmph_search(hash, tag.c_str(), tag.size()) + 1;
        if(nodes[id] == nodetype::D) {
          dead_seqs << id << '\t' << seq << '\n';
        } else {
          alive_seqs << id << '\t' << seq << '\n';
          ++alive_nodes;
        }
      }
        break;
      default:
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        break;
      }
    }
  }

  PRINT_NOW(" ↳ Alive nodes : " << alive_nodes << '\n');
  PRINT_NOW(" ↳ Total nodes : " << node_num -1 << '\n');

  if(alive_nodes == 0) {
    PRINT_NOW("  ↳ [ATTENTION] No nodes are \"alive\" in the graph. Maybe you set the minimum length of the tip as too short?\n");
  }

  PRINT_NOW(" ↳ Building the graph\n");
  // Create edges
  {
    char record_type;
    ifstream input(gfafilename.c_str());
    ofstream ltolf(ltolfilename.c_str());
    ofstream dkckm(deadkckmfilename.c_str());
    while(input >> record_type) {
      switch(record_type) {
      case 'S': {
        string tag;
        size_t id;
        string seq;
        input >> tag >> seq;
        id = cmph_search(hash, tag.c_str(), tag.size()) + 1;
        string opts;
        size_t kc = lengths[id];
        float km = 1.0;
        getline(input, opts);
        istringstream optsss(opts);
        string token;
        while(optsss >> token) {
          if(token.find("km:f:") != string::npos) {
	    size_t start = token.find("km:f:") + 5;
            km = stof(token.substr(start));
          } else if(token.find("KC:i:") != string::npos) {
	    size_t start = token.find("KC:i:") + 5;
            kc = stoi(token.substr(start));
          }
        }
        if(nodes[id] == nodetype::D) {
          dkckm << id << "\t" << kc << "\t" << km << "\n";
        } else {
          g[id].set_kc(kc);
          g[id].set_km(km);
        }
      }
        break;
      case 'L': {
        string tagu, tagv;
        size_t u_id, v_id;
        char u_or, v_or;
        input >> tagu >> u_or >> tagv >> v_or;
        u_id = cmph_search(hash, tagu.c_str(), tagu.size()) + 1;
        v_id = cmph_search(hash, tagv.c_str(), tagv.size()) + 1;
        if(nodes[u_id] == nodetype::S || nodes[v_id] == nodetype::S) {
          // S -> L or L -> S edge. Add it
          g[u_id].set_length(lengths[u_id]);
          g[v_id].set_length(lengths[v_id]);
          nodeend endu, endv;
          if(bool_rc(u_or)) endu = nodeend::RIGHT;
          else endu = nodeend::LEFT;
          if(bool_rc(v_or)) endv = nodeend::LEFT;
          else endv = nodeend::RIGHT;
          g[u_id].add_edge(endu, v_id, endv);
          g[v_id].add_edge(endv, u_id, endu);
        } else {
          // L -> L edge
          // Save it for visiting the graph at the end
          if(bool_rc(u_or)) u_or = 'R';
          else u_or = 'L';
          if(bool_rc(v_or)) v_or = 'L';
          else v_or = 'R';
          ltolf << u_id << '\t' << v_id << '\t' << u_or << '\t' << v_or << endl;
          if(nodes[u_id] != nodetype::D || nodes[v_id] != nodetype::D) {
            // L -> L edge with either one of the two in the graph
            if(nodes[u_id] != nodetype::D) {
              // u_id is "long", is in the graph, and is connected to another "long" node
              // (either in or out the graph)
              g[u_id].set_length(lengths[u_id]);
              if(char_to_end(u_or) == nodeend::RIGHT)  g[u_id].block_right();
              else g[u_id].block_left();
            }
            if(nodes[v_id] != nodetype::D) {
              // v_id is "long", is in the graph, and is connected to another "long" node
              // (either in or out the graph)
              g[v_id].set_length(lengths[v_id]);
              if(char_to_end(v_or) == nodeend::LEFT)  g[v_id].block_left();
              else g[v_id].block_right();
            }
          }
        }
        input.ignore(numeric_limits<streamsize>::max(), '\n');
      }
        break;
      default:
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        break;
      }
    }
  }

  PRINT_NOW(" ↳ Saving hash function\n");
  FILE* hashfile = fopen(hashfilename.c_str(), "wb");
  cmph_dump(hash, hashfile);
  fclose(hashfile);

  PRINT_NOW(" ↳ Saving tag of nodes for hash functions\n");
  {
    size_t byte_size = nodes.size() / 8;
    if(nodes.size() % 8) ++byte_size;
    vector<char> bitv(byte_size);
    for(size_t i = 0; i < nodes.size(); ++i) {
      if(nodes[i] != nodetype::D) {
        bitv[i/8] |= (0b1 << i%8);
      }
    }
    ofstream deletedfile(deletedhashfilename.c_str(), ios::binary);
    deletedfile.write((char*)&bitv[0], bitv.size());
  }

  PRINT_NOW(" ↳ Destroy MPHFs\n");
  cmph_destroy(hash);
  PRINT_NOW("GFA read end\n");

  return true;
}

bool GFA::read_structure_gfa_and_fasta(const string &gfafilename,
                                       const string &fastafilename,
                                       const unsigned int &ksize,
                                       const unsigned int &maxk,
                                       Graph &g,
                                       bool debug) {
  PRINT_NOW("GFA read begin\n");

  // Read sequences tags and create a MPHF for them
  PRINT_NOW(" ↳ Creating tags file\n");
  gzFile fp = gzopen(fastafilename.c_str(), "r");
  kseq_t *seq = kseq_init(fp);
  int l, node_num=1;
  ofstream tagsfile(keysfilename.c_str());
  while((l = kseq_read(seq)) >= 0) {
    tagsfile << seq->name.s << '\n';
    ++node_num;
  }
  tagsfile.close();
  kseq_destroy(seq);
  gzclose(fp);

  PRINT_NOW(" ↳ Computing MPHF of tags\n");
  cmph_t *hash = build_mphf_em(keysfilename);
  if(hash == NULL) {
    PRINT_NOW("  ↳ Error while building MPHF of keys in file " << keysfilename << "... aborting\n");
    return false;
  }

  vector<nodetype> nodes(node_num, nodetype::D);
  vector<tiplen_t> lengths(node_num);
  g.resize(node_num);

  PRINT_NOW(" ↳ Computing nodetypes\n");
  fp = gzopen(fastafilename.c_str(), "r");
  seq = kseq_init(fp);
  size_t id;
  while((l = kseq_read(seq)) >= 0) {
    id = cmph_search(hash, seq->name.s, seq->name.l) + 1;
    lengths[id] = seq->seq.l <= numeric_limits<tiplen_t>::max() ? seq->seq.l : numeric_limits<tiplen_t>::max();
    if(seq->seq.l <= maxk) {
      nodes[id] = nodetype::S;
    }
  }
  kseq_destroy(seq);
  gzclose(fp);

  // mark long nodes next to short nodes
  {
    char record_type;
    ifstream input(gfafilename.c_str());
    while(input >> record_type) {
      switch(record_type) {
      case 'L': {
        string tagu, tagv;
        size_t idu, idv;
        char u_or, v_or;
        input >> tagu >> u_or >> tagv >> v_or;
        idu = cmph_search(hash, tagu.c_str(), tagu.size()) + 1;
        idv = cmph_search(hash, tagv.c_str(), tagv.size()) + 1;
        if(nodes[idu] == nodetype::S || nodes[idv] == nodetype::S) {
          nodes[idu] = min(nodes[idu], nodetype::LNS);
          nodes[idv] = min(nodes[idv], nodetype::LNS);
        }
        input.ignore(numeric_limits<streamsize>::max(), '\n');
      }
        break;
      default:
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        break;
      }
    }
  }

  PRINT_NOW(" ↳ Saving \"dead\" sequences\n");
  fp = gzopen(fastafilename.c_str(), "r");
  seq = kseq_init(fp);
  ofstream dead_seqs(deadseqfilename.c_str());
  ofstream alive_seqs(aliveseqfilename.c_str());
  size_t alive_nodes = 0;
  while((l = kseq_read(seq)) >= 0) {
    id = cmph_search(hash, seq->name.s, seq->name.l) + 1;
    if(nodes[id] == nodetype::D) {
      dead_seqs << id << '\t' << seq->seq.s << '\n';
    } else {
      alive_seqs << id << '\t' << seq->seq.s << '\n';
      ++alive_nodes;
    }
  }
  dead_seqs.close();
  alive_seqs.close();
  kseq_destroy(seq);
  gzclose(fp);

  PRINT_NOW(" ↳ Alive nodes : " << alive_nodes << '\n');
  PRINT_NOW(" ↳ Total nodes : " << node_num -1 << '\n');

  if(alive_nodes == 0) {
    PRINT_NOW("  ↳ [ATTENTION] No nodes are \"alive\" in the graph. Maybe you set the minimum length of the tip as too short?\n");
    // PRINT_NOW("  ↳ destroy MPHFs\n");
    // cmph_destroy(hash);
    // return false;
  }

  PRINT_NOW(" ↳ Building the graph\n");
  // Create edges
  {
    char record_type;
    ifstream input(gfafilename.c_str());
    ofstream ltolf(ltolfilename.c_str());
    ofstream dkckm(deadkckmfilename.c_str());
    while(input >> record_type) {
      switch(record_type) {
      case 'S': {
        string tag;
        size_t id;
        string seq;
        input >> tag >> seq;
        id = cmph_search(hash, tag.c_str(), tag.size()) + 1;
        string opts;
        size_t kc = lengths[id];
        float km = 1.0;
        getline(input, opts);
        istringstream optsss(opts);
        string token;
        while(optsss >> token) {
          if(token.find("km:f:") == 0) {
            km = stof(token.substr(5));
          } else if(token.find("KC:i:") == 0) {
            kc = stoi(token.substr(5));
          }
        }
        if(nodes[id] == nodetype::D) {
          dkckm << id << "\t" << kc << "\t" << km << "\n";
        } else {
          g[id].set_kc(kc);
          g[id].set_km(km);
        }
      }
        break;
      case 'L': {
        string tagu, tagv;
        size_t u_id, v_id;
        char u_or, v_or;
        input >> tagu >> u_or >> tagv >> v_or;
        u_id = cmph_search(hash, tagu.c_str(), tagu.size()) + 1;
        v_id = cmph_search(hash, tagv.c_str(), tagv.size()) + 1;
        if(nodes[u_id] == nodetype::S || nodes[v_id] == nodetype::S) {
          // S -> L or L -> S edge. Add it
          g[u_id].set_length(lengths[u_id]);
          g[v_id].set_length(lengths[v_id]);
          nodeend endu, endv;
          if(bool_rc(u_or)) endu = nodeend::RIGHT;
          else endu = nodeend::LEFT;
          if(bool_rc(v_or)) endv = nodeend::LEFT;
          else endv = nodeend::RIGHT;
          g[u_id].add_edge(endu, v_id, endv);
          g[v_id].add_edge(endv, u_id, endu);
        } else {
          // L -> L edge
          // Save it for visiting the graph at the end
          if(bool_rc(u_or)) u_or = 'R';
          else u_or = 'L';
          if(bool_rc(v_or)) v_or = 'L';
          else v_or = 'R';
          ltolf << u_id << '\t' << v_id << '\t' << u_or << '\t' << v_or << endl;
          if(nodes[u_id] != nodetype::D || nodes[v_id] != nodetype::D) {
            // L -> L edge with either one of the two in the graph
            if(nodes[u_id] != nodetype::D) {
              // u_id is "long", is in the graph, and is connected to another "long" node
              // (either in or out the graph)
              g[u_id].set_length(lengths[u_id]);
              if(char_to_end(u_or) == nodeend::RIGHT)  g[u_id].block_right();
              else g[u_id].block_left();
            }
            if(nodes[v_id] != nodetype::D) {
              // v_id is "long", is in the graph, and is connected to another "long" node
              // (either in or out the graph)
              g[v_id].set_length(lengths[v_id]);
              if(char_to_end(v_or) == nodeend::LEFT)  g[v_id].block_left();
              else g[v_id].block_right();
            }
          }
        }
        input.ignore(numeric_limits<streamsize>::max(), '\n');
      }
        break;
      default:
        input.ignore(numeric_limits<streamsize>::max(), '\n');
        break;
      }
    }
  }

  PRINT_NOW(" ↳ Saving hash function\n");
  FILE* hashfile = fopen(hashfilename.c_str(), "wb");
  cmph_dump(hash, hashfile);
  fclose(hashfile);

  PRINT_NOW(" ↳ Saving tag of nodes for hash functions\n");
  {
    size_t byte_size = nodes.size() / 8;
    if(nodes.size() % 8) ++byte_size;
    vector<char> bitv(byte_size);
    for(size_t i = 0; i < nodes.size(); ++i) {
      if(nodes[i] != nodetype::D) {
        bitv[i/8] |= (0b1 << i%8);
      }
    }
    ofstream deletedfile(deletedhashfilename.c_str(), ios::binary);
    deletedfile.write((char*)&bitv[0], bitv.size());
  }

  PRINT_NOW(" ↳ Destroy MPHFs\n");
  cmph_destroy(hash);
  PRINT_NOW("GFA read end\n");

  return true;
}
