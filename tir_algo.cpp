#include "tir_algo.hpp"

smallnode::smallnode()
  : len(0), left_end_edges(), right_end_edges(),
    block_left(false), block_right(false) {
}

smallnode::smallnode(const smallnode &other)
  : len(other.len), left_end_edges(other.right_end_edges), right_end_edges(other.left_end_edges),
    block_left(other.block_left), block_right(other.block_right) {
}    

void smallnode::update_edge(const uint32_t &old_id, const uint32_t &new_id,
                            const bool new_id_rev) {
  for(auto &x : left_end_edges) {
    if(x.first == old_id) {
      x.first = new_id;
      if(new_id_rev) x.second = nodeend_complement(x.second);
    }
  }
  for(auto &x : right_end_edges) {
    if(x.first == old_id) {
      x.first = new_id;
      if(new_id_rev) x.second = nodeend_complement(x.second);
    }
  }
}

void remove_tips(const vector<uint32_t> &cc, Graph &g,
                 const unsigned int &ksize, const unsigned int &kmax) {
  stringstream operations;
  if(cc.size() == 0) {
    return;
  }
  size_t cc_id = *min_element(cc.begin(), cc.end());
  if(cc.size() == 1 && g[cc[0]].length() <= kmax) {
    output_remove(operations, cc[0]);
    stringstream filename;
    size_t cc_group = cc_id % max_partition_num;
    filename << wk_files_prefix << cc_group << merge_ops_file_suffix;
    ofstream outf(filename.str(), ios::app);
    outf << operations.str();
    return;
  }
  smallgraph cg;
  for(auto &id : cc) {
    cg.nodes[id] = smallnode();
    cg.nodes[id].len = g[id].length();
    cg.nodes[id].left_end_edges = g[id].left_edges();
    cg.nodes[id].right_end_edges = g[id].right_edges();
    cg.nodes[id].block_left = !g[id].can_merge_left();
    cg.nodes[id].block_right = !g[id].can_merge_right();
    cg.nodes[id].km = g[id].get_km();
    cg.nodes[id].kc = g[id].get_kc();
  }
  for(size_t len = ksize; len <= kmax; ++len) {
    // Find sources and sinks of length len
    vector<uint32_t> tips = find_tips_of_length(cg, len);
    vector<pair<uint32_t, nodeend> > to_test;

    for(auto &id : tips) {
      // Remove isolated nodes
      if(cg.nodes[id].right_end_edges.size() == 0 && cg.nodes[id].left_end_edges.size() == 0) {
        output_remove(operations, id);
      } else if(!tip_can_merge(cg, id)) {
        // Manage tips that become mergeable
        // remove node
        if(cg.nodes[id].right_end_edges.size() == 0) {
          vector<pair<uint32_t, nodeend> > to_test1 = remove_edges(cg, cg.nodes[id].left_end_edges, id);
          if(to_test1.size() > 0)
            to_test.insert(to_test.end(), to_test1.begin(), to_test1.end());
        } else {
          vector<pair<uint32_t, nodeend> > to_test1 = remove_edges(cg, cg.nodes[id].right_end_edges, id);
          if(to_test1.size() > 0)
            to_test.insert(to_test.end(), to_test1.begin(), to_test1.end());
        }
        output_remove(operations, id);
        clear_edges(cg, id);
      }
    }
    map<uint32_t, pair<bool, uint32_t> > rename;
    for(auto &id : to_test) {
      while(rename.find(id.first) != rename.end()) {
        if(rename[id.first].first)
          id.second = nodeend_complement(id.second);
        id.first = rename[id.first].second;
      }
#ifndef NDEBUG
      output_test(operations, id.second);
#endif
      if(id.second == nodeend::LEFT &&
         cg.nodes[id.first].left_end_edges.size() == 1 &&
         !cg.nodes[id.first].block_left) {
        // merge left end
        uint32_t target = cg.nodes[id.first].left_end_edges[0].first;
        if(target != id.first) {
          pair<bool, nodeend> r = can_merge(cg.nodes[target], id.first, id.second);
          if(r.first) {
            // node can merge
            output_merge(operations, id.first, 'L', target, end_to_char(r.second));
            merge_nodes(cg, id.first, nodeend::LEFT, target, r.second, ksize -1, rename);
          }
        }
      } else if(id.second == nodeend::RIGHT &&
                cg.nodes[id.first].right_end_edges.size() == 1 &&
                !cg.nodes[id.first].block_right) {
        // merge right end
        uint32_t target = cg.nodes[id.first].right_end_edges[0].first;
        if(target != id.first) {
          pair<bool, nodeend> r = can_merge(cg.nodes[target], id.first, id.second);
          if(r.first) {
            // node can merge
            output_merge(operations, id.first, 'R', target, end_to_char(r.second));
            merge_nodes(cg, id.first, nodeend::RIGHT, target, r.second, ksize -1, rename);
          }
        }
      }
    }
  }
  // Output KC and KM
  ofstream outkckm(alivekckmfilename.c_str(), ios::app);
  for(const auto &n : cg.nodes) {
    outkckm << n.first << "\t" << n.second.kc << "\t" << n.second.km << "\n";
  }
  stringstream filename;
  size_t cc_group = cc_id % max_partition_num;
  filename << wk_files_prefix << cc_group << merge_ops_file_suffix;
  ofstream outf(filename.str(), ios::app);
  outf << operations.str();
  ofstream oute(intraccs_edges_filename.c_str(), ios::app);
  for(const auto& current_node : cg.nodes) {
    for(const auto& target_node : current_node.second.right_end_edges) {
      if(current_node.first < target_node.first) {
        if(target_node.second == nodeend::LEFT) {
          oute << current_node.first << "\t" << target_node.first << "\t" << 'R'  << "\t" << 'L' << "\n";
        } else {
          oute << current_node.first << "\t" << target_node.first << "\t" << 'R' << "\t" << 'R' << "\n";
        }
      }
    }
    for(const auto& target_node : current_node.second.left_end_edges) {
      if(current_node.first < target_node.first) {
        if(target_node.second == nodeend::LEFT) {
          oute << current_node.first << "\t" << target_node.first << "\t" << 'L' << "\t" << 'L' << "\n";
        } else {
          oute << current_node.first << "\t" << target_node.first << "\t" << 'L' << "\t" << 'R' << "\n";
        }
      }
    }
  }
}

bool tip_can_merge(smallgraph &cg, uint32_t &id) {
  smallnode & node = cg.nodes[id];
  if(node.left_end_edges.size() == 1) {
    uint32_t otherid = node.left_end_edges[0].first;
    nodeend other_end = node.left_end_edges[0].second;
    auto ret = can_merge(cg.nodes[otherid], id, nodeend::LEFT);
    return (ret.first && ret.second == other_end);
  } else if (node.right_end_edges.size() == 1) {
    uint32_t otherid = node.right_end_edges[0].first;
    nodeend other_end = node.right_end_edges[0].second;
    auto ret = can_merge(cg.nodes[otherid], id, nodeend::RIGHT);
    return (ret.first && ret.second == other_end);
  }
  return false;
}

void merge_nodes(smallgraph &cg, const uint32_t &firstid, const nodeend &firstend,
                 const uint32_t &secondid, const nodeend &secondend,
                 const size_t &overlap, map<uint32_t, pair<bool, uint32_t> > &rename) {
  bool second_is_reversed = false;
  smallnode new_node;
  new_node.len = cg.nodes[firstid].len + cg.nodes[secondid].len - overlap;
  new_node.kc = cg.nodes[firstid].kc + cg.nodes[secondid].kc;
  new_node.km = (cg.nodes[firstid].len * cg.nodes[firstid].km + cg.nodes[secondid].len * cg.nodes[secondid].km) / (cg.nodes[firstid].len + cg.nodes[secondid].len);
  if(firstend == nodeend::LEFT) {
    new_node.right_end_edges = cg.nodes[firstid].right_end_edges;
    new_node.block_right = cg.nodes[firstid].block_right;
    if(secondend == nodeend::RIGHT) {
      new_node.left_end_edges = cg.nodes[secondid].left_end_edges;
      new_node.block_left = cg.nodes[secondid].block_left;
    } else {
      new_node.left_end_edges = cg.nodes[secondid].right_end_edges;
      new_node.block_left = cg.nodes[secondid].block_right;
      second_is_reversed = true;
    }
  } else {
    new_node.left_end_edges = cg.nodes[firstid].left_end_edges;
    new_node.block_left = cg.nodes[firstid].block_left;
    if(secondend == nodeend::LEFT) {
      new_node.right_end_edges = cg.nodes[secondid].right_end_edges;
      new_node.block_right = cg.nodes[secondid].block_right;
    } else {
      new_node.right_end_edges = cg.nodes[secondid].left_end_edges;
      new_node.block_right = cg.nodes[secondid].block_left;
      second_is_reversed = true;
    }
  }
  for(auto &target : new_node.left_end_edges)
    cg.nodes[target.first].update_edge(secondid, firstid, second_is_reversed);
  for(auto &target : new_node.right_end_edges)
    cg.nodes[target.first].update_edge(secondid, firstid, second_is_reversed);
  cg.nodes.erase(firstid);
  cg.nodes.erase(secondid);
  rename[secondid] = make_pair(second_is_reversed, firstid);
  cg.nodes[firstid] = new_node;
}

pair<bool, nodeend> can_merge(const smallnode &node, const uint32_t target,
                              const nodeend &other_end) {
  bool can_merge = false;
  nodeend end = nodeend::LEFT; // avoid compiler warning
  if(!node.block_left && node.left_end_edges.size() == 1 &&
     node.left_end_edges[0] == make_pair(target, other_end)) {
    can_merge = true;
    end = nodeend::LEFT;
  } else if(!node.block_right && node.right_end_edges.size() == 1 &&
            node.right_end_edges[0] == make_pair(target, other_end)) {
    can_merge = true;
    end = nodeend::RIGHT;
  }
  return make_pair(can_merge, end);
}

bool clear_edges(smallgraph &cg, const uint32_t &id) {
  cg.nodes[id].right_end_edges.clear();
  cg.nodes[id].left_end_edges.clear();
  return true;
}

bool remove_from_list(vector<pair<uint32_t, nodeend> > &edges, const uint32_t &id,
                      const nodeend &other_end) {
  auto it = find(edges.begin(), edges.end(), make_pair(id, other_end));
  if(it != edges.end()) {
    edges.erase(it);
    return true;
  }
  return false;
}

vector<pair<uint32_t, nodeend> > remove_edges(smallgraph &cg,
                                              vector<pair<uint32_t, nodeend> > &removed_edges,
                                              const uint32_t &source_id) {
  vector<pair<uint32_t, nodeend> > nodes_to_test;
  nodeend tip_end_used = cg.nodes[source_id].right_end_edges.size() == 0 ? nodeend::LEFT : nodeend::RIGHT;
  for(auto &target : removed_edges) {
    if(target.second == nodeend::RIGHT) {
      remove_from_list(cg.nodes[target.first].right_end_edges, source_id, tip_end_used);
      nodes_to_test.push_back(make_pair(target.first, nodeend::RIGHT));
    } else {
      remove_from_list(cg.nodes[target.first].left_end_edges, source_id, tip_end_used);
      nodes_to_test.push_back(make_pair(target.first, nodeend::LEFT));
    }
  }
  return nodes_to_test;
}

vector<uint32_t> find_tips_of_length(const smallgraph &cg, const size_t &len) {
  vector<uint32_t> ret;
  for(auto const &x : cg.nodes) {
    if((x.second.left_end_edges.size() == 0 || x.second.right_end_edges.size() == 0)
       && x.second.len == len) {
      ret.push_back(x.first);
    }
  }
  return ret;
}

tuple<uint32_t, uint32_t, nodeend, nodeend>
find_mergeable_edge(smallgraph &cg) {
  uint32_t id1, id2;
  nodeend end2;
  for(auto const &x : cg.nodes) {
    id1 = x.first;
    if(x.second.left_end_edges.size() == 1) {
      id2 = x.second.left_end_edges[0].first;
      end2 = x.second.left_end_edges[0].second;
      if(end2 == nodeend::LEFT &&
         cg.nodes[id2].left_end_edges[0].first == x.first &&
         cg.nodes[id2].left_end_edges[0].second == nodeend::LEFT)
        return make_tuple(id1, id2, nodeend::LEFT, nodeend::LEFT);
      else if (end2 == nodeend::RIGHT &&
         cg.nodes[id2].right_end_edges[0].first == x.first &&
         cg.nodes[id2].right_end_edges[0].second == nodeend::LEFT)
        return make_tuple(id1, id2, nodeend::LEFT, nodeend::RIGHT);
    }
    if(x.second.right_end_edges.size() == 1) {
      id2 = x.second.right_end_edges[0].first;
      end2 = x.second.right_end_edges[0].second;
      if(end2 == nodeend::LEFT &&
         cg.nodes[id2].left_end_edges[0].first == x.first &&
         cg.nodes[id2].left_end_edges[0].second == nodeend::RIGHT)
        return make_tuple(id1, id2, nodeend::RIGHT, nodeend::LEFT);
      else if(end2 == nodeend::RIGHT &&
              cg.nodes[id2].right_end_edges[0].first == x.first &&
              cg.nodes[id2].right_end_edges[0].second == nodeend::RIGHT)
        return make_tuple(id1, id2, nodeend::RIGHT, nodeend::RIGHT);
    }
  }
  return make_tuple(0, 0, nodeend::LEFT, nodeend::LEFT);
}

// pair<bool, nodeend> test_if_single_edge(smallgraph &cg,
//                                         const uint32_t &id1, const uint32_t &id2) {
//   if(cg.nodes[id1].left_end_edges.size() == 1 && cg.nodes[id1].left_end_edges[0].first == id2)
//     return make_pair(true, nodeend::LEFT);
//   if(cg.nodes[id1].right_end_edges.size() == 1 && cg.nodes[id1].right_end_edges[0].first == id2)
//     return make_pair(true, nodeend::RIGHT);
//   return make_pair(false, nodeend::LEFT);
// }

void output_single_arg(stringstream &o, const char &op, const uint32_t &id) {
  o << op << "\t" << id << '\n';
}

void output_remove(stringstream &o, const uint32_t &id) {
  output_single_arg(o, 'R', id);
}

void output_test(stringstream &o, const uint32_t &id) {
  output_single_arg(o, 'T', id);
}

void output_merge(stringstream &o, const uint32_t &firstid, const char &firstend,
                  const uint32_t &secondid, const char &secondend) {
  o << "M\t" << firstid << '\t' << firstend << "\t" << secondid << '\t' << secondend << '\n';
}

map<size_t, pair<bool, size_t> >
merge_and_remove(const vector<vector<uint32_t>> &ccs, const string &fastafilename,
                 const size_t &alive_num, const size_t &min_overlap) {
  // Load mphf
  PRINT_NOW(" ↳ Loading MPHF from file\n")
  cmph_t *lvl1hf;
  lvl1hf = load_mphf(hashfilename);
  if(lvl1hf == NULL) {
    PRINT_NOW("   ↳ [ERROR] error loading " << hashfilename << "... aborting\n");
    abort();
  }

  ifstream alivefile(deletedhashfilename.c_str(), ios::binary | ios::ate);
  vector<char> alive(alivefile.tellg());
  alivefile.seekg(0, ios::beg);
  alivefile.read(alive.data(), alive.size());
  alivefile.close();

  // Partition unitigs
  vector<uint32_t> reverseccs(alive_num, 0);
  for(size_t i = 0; i < ccs.size(); ++i) {
    if(ccs[i].size() == 0) continue;
    uint32_t cc_id = *min_element(ccs[i].begin(), ccs[i].end());
    for(size_t j = 0; j < ccs[i].size(); ++j) {
      reverseccs[ccs[i][j]] = cc_id;
    }
  }

  {
    PRINT_NOW(" ↳ Partitioning unitigs sequences\n");
    ifstream alive_seq(aliveseqfilename.c_str());
    size_t id;
    string seq;
    while(alive_seq >> id) {
      alive_seq >> seq;
      if(ccs[reverseccs[id]].size() <= cc_max_size) {
        stringstream filename;
        size_t cc_group = reverseccs[id] % max_partition_num;
        filename << wk_files_prefix << cc_group << seqs_file_suffix;
        ofstream outf(filename.str(), ios::app);
        outf << id << '\t' << seq << '\n';
      }
    }
  }

  // destroy hash functions
  cmph_destroy(lvl1hf);

  map<size_t, pair<bool, size_t>> node_rename;

  PRINT_NOW(" ↳ Merging sequences\n");
  for(size_t i = 0; i < max_partition_num; ++i) {
    // Load sequences
    map<uint32_t, string> sequences;
    {
      stringstream seqfilename;
      seqfilename << wk_files_prefix << i << seqs_file_suffix;
      ifstream seqfile(seqfilename.str());
      uint32_t id;
      string seq;
      while(seqfile >> id) {
        seqfile >> seq;
        sequences[id] = seq;
      }
    }
    if(sequences.size() == 0)
      continue;
    stringstream outmergedfilename;
    outmergedfilename << wk_files_prefix << i << ".tipped.fa";
    ofstream outmerged(outmergedfilename.str());
    // Performa operations
    stringstream opsfilename;
    opsfilename << wk_files_prefix << i << merge_ops_file_suffix;
    ifstream opsfile(opsfilename.str());
    char op;
    while(opsfile >> op) {
#ifndef NDEBUG
      if(op == 'T') {
        uint32_t id;
        opsfile >> id;
      } else
#endif
      if(op == 'R') {
        uint32_t id;
        opsfile >> id;
        sequences.erase(id);
      } else {
        uint32_t id1, id2;
        char end1, end2;
        opsfile >> id1 >> end1 >> id2 >> end2;
        node_rename[id2] = make_pair(false, id1);
        if(end1 == end2) {
          rc_seq(sequences[id2]);
          node_rename[id2].first = true;
        }
        if(end1 == 'L') {
          sequences[id2] = sequences[id2].substr(0, sequences[id2].size() - min_overlap);
          sequences[id2].append(sequences[id1]);
          sequences[id1] = std::move(sequences[id2]);
        } else {
          sequences[id1].append(sequences[id2].substr(min_overlap));
        }
        sequences.erase(id2);
      }
    }
    for(auto & it : sequences) {
      outmerged << it.first << '\t' << it.second << '\n';
    }
  }
  return node_rename;
}


void rc_seq(string &seq) {
  reverse(seq.begin(), seq.end());
  for(size_t i = 0; i < seq.size(); ++i)
    seq[i] = rc_char(seq[i]);
}

char rc_char(char &c) {
  return COMPLEMENTS[(uint8_t)c];
}

bool test_alive(const vector<char> &bv, const size_t &id) {
  return (bv[id/8] & (0b1 << id % 8));
}

void merge_l_to_l(map<size_t, pair<bool, size_t> > &rename, const size_t &min_overlap, const bool double_link = false) {
  ListGraph g;
  std::map<uint32_t, int>  node_id;
  ListGraph::NodeMap<string> sequences_map(g);
  ListGraph::NodeMap<size_t> kc_map(g);
  ListGraph::NodeMap<float> km_map(g);
  ListGraph::EdgeMap<pair<char, char> > edge_map(g);

  PRINT_NOW(" ↳ Load \"deleted\" sequences\n");
  {
    ifstream dead_seqs(deadseqfilename.c_str());
    uint32_t id;
    string seq;
    while(dead_seqs >> id) {
      dead_seqs >> seq;
      ListGraph::Node a = g.addNode();
      node_id[id] = g.id(a);
      sequences_map[a] = seq;
    }
    ifstream deadkckm(deadkckmfilename.c_str());
    size_t kc;
    float km;
    while(deadkckm >> id) {
      deadkckm >> kc;
      deadkckm >> km;
      ListGraph::Node a = g.nodeFromId(node_id[id]);
      kc_map[a] = kc;
      km_map[a] = km;
    }
  }

  PRINT_NOW(" ↳ Load \"tipped\" sequences\n");
  for(size_t i = 0; i < max_partition_num; i++) {
    stringstream filename;
    filename << wk_files_prefix << i << ".tipped.fa";
    ifstream inputseqs(filename.str());
    uint32_t id;
    string seq;
    while(inputseqs >> id) {
      inputseqs >> seq;
      ListGraph::Node a = g.addNode();
      node_id[id] = g.id(a);
      sequences_map[a] = seq;
    }
  }

  {
    ifstream alivekckm(alivekckmfilename.c_str());
    size_t kc;
    float km;
    uint32_t id;
    while(alivekckm >> id) {
      alivekckm >> kc;
      alivekckm >> km;
      if(node_id.find(id) != node_id.end()) {
        ListGraph::Node a = g.nodeFromId(node_id[id]);
        kc_map[a] = kc;
        km_map[a] = km;
      }
    }
  }

  ifstream ltolf(ltolfilename.c_str());

  uint32_t id1, id2;
  char end1, end2;

  PRINT_NOW(" ↳ Load inter-ccs edges\n");
  while(ltolf >> id1 >> id2 >> end1 >> end2) {
    while(rename.find(id1) != rename.end()) {
      auto e = rename.find(id1)->second;
      id1 = e.second;
      if(e.first) end1 = end1 == 'R' ? 'L' : 'R';
    }
    while(rename.find(id2) != rename.end()) {
      auto e = rename.find(id2)->second;
      id2 = e.second;
      if(e.first) end2 = end2 == 'R' ? 'L' : 'R';
    }
    if(node_id[id1] < node_id[id2]) {
      swap(id1, id2);
      swap(end1, end2);
    }
    ListGraph::Edge e = g.addEdge(g.nodeFromId(node_id[id1]),
                                  g.nodeFromId(node_id[id2]));
    edge_map[e] = make_pair(end1, end2);
  }

  // stringstream edges_filename;
  // edges_filename << wk_files_prefix << "edges.txt";
  ifstream edgesf(intraccs_edges_filename.c_str());

  PRINT_NOW(" ↳ Load intra-ccs edges\n");
  while(edgesf >> id1 >> id2 >> end1 >> end2) {
    while(rename.find(id1) != rename.end()) {
      auto e = rename.find(id1)->second;
      id1 = e.second;
      if(e.first) end1 = end1 == 'R' ? 'L' : 'R';
    }
    while(rename.find(id2) != rename.end()) {
      auto e = rename.find(id2)->second;
      id2 = e.second;
      if(e.first) end2 = end2 == 'R' ? 'L' : 'R';
    }
    if(node_id[id1] < node_id[id2]) {
      swap(id1, id2);
      swap(end1, end2);
    }
    ListGraph::Edge e = g.addEdge(g.nodeFromId(node_id[id1]),
                                  g.nodeFromId(node_id[id2]));
    edge_map[e] = make_pair(end1, end2);
  }

  // Contract the graph
  PRINT_NOW(" ↳ Contract the graph\n");
  // TODO: check whether we need the fourth element
  // 1. node_id 2. is leftmost one 3. is rightmost one 4. is reversed
  map<int, tuple<int, bool, bool, bool> > node_id_to_unitig_id;
  map<int, vector<pair<int, bool > > > contracted_paths;
  size_t max_unitig_id = 0;
  unordered_set<int> node_deleted;

  for(ListGraph::NodeIt n(g); n != INVALID; ++n) {
    if(node_deleted.find(g.id(n)) == node_deleted.end()) {
      node_deleted.insert(g.id(n));
      vector<pair<ListGraph::Node, char> > right_neigh, left_neigh;
      for(ListGraph::IncEdgeIt e(g, n); e != INVALID; ++e) {
        auto dest_node = g.target(e);
        char current_node_end, other_node_end;
        tie(current_node_end, other_node_end) = edge_map[e];
        if(g.id(n) < g.id(dest_node)) swap(current_node_end, other_node_end);
        if(current_node_end == 'R')
          right_neigh.push_back(make_pair(dest_node, other_node_end));
        else
          left_neigh.push_back(make_pair(dest_node, other_node_end));
      }
      vector<pair<ListGraph::Node, char> > right_extend, left_extend;
      if(right_neigh.size() == 1) {
        right_extend = get_simple_path_from(g, right_neigh[0].first, edge_map, right_neigh[0].second, node_deleted);
      }
      if(left_neigh.size() == 1) {
        left_extend = get_simple_path_from(g, left_neigh[0].first, edge_map, left_neigh[0].second, node_deleted);
        reverse(left_extend.begin(), left_extend.end());
      }
      size_t node_pos_in_path = 0;

      for(size_t index = 0; index < left_extend.size(); ++index) {
        node_id_to_unitig_id[g.id(left_extend[index].first)] = make_tuple(max_unitig_id, node_pos_in_path == 0, false, left_extend[index].second == 'L');
        contracted_paths[max_unitig_id].push_back(make_pair(g.id(left_extend[index].first), left_extend[index].second == 'L'));
        ++node_pos_in_path;
      }

      node_id_to_unitig_id[g.id(n)] = make_tuple(max_unitig_id, node_pos_in_path == 0, node_pos_in_path == left_extend.size() + right_extend.size(), false);
      ++node_pos_in_path;
      contracted_paths[max_unitig_id].push_back(make_pair(g.id(n), false));

      for(size_t index = 0; index < right_extend.size(); ++index) {
        node_id_to_unitig_id[g.id(right_extend[index].first)] = make_tuple(max_unitig_id, false, node_pos_in_path == left_extend.size() + right_extend.size(), right_extend[index].second == 'R');
        contracted_paths[max_unitig_id].push_back(make_pair(g.id(right_extend[index].first), right_extend[index].second == 'R'));
        ++node_pos_in_path;
      }

      max_unitig_id++;
    }
  }

  struct gfa_edge {
    gfa_edge(size_t s_id, size_t t_id, char s_end, char t_end)
      : source_id(s_id), target_id(t_id), source_end(s_end), target_end(t_end) {
    }
    size_t source_id, target_id;
    bool source_end, target_end;
  };

  vector< gfa_edge > gfa_edges;

  PRINT_NOW(" ↳ Output GFA\n");
  // TODO: Check the header line
  cout << "H\tVN:Z:1.0\n";
  for(size_t unitig_id = 0; unitig_id < max_unitig_id; ++unitig_id) {
    ListGraph::Node left_end = g.nodeFromId(contracted_paths[unitig_id][0].first);
    size_t this_unitig_id = get<0>(node_id_to_unitig_id[g.id(left_end)]);
    bool this_is_reversed = get<3>(node_id_to_unitig_id[g.id(left_end)]);
    char this_junction_end = this_is_reversed ? 'R' : 'L'; // Consider only edges attached to this end
    for(ListGraph::IncEdgeIt e(g, left_end); e != INVALID; ++e ) {
      ListGraph::Node dest_node = g.u(e);
      if(g.id(dest_node) == g.id(left_end)) dest_node = g.v(e);
      char current_end, other_end;
      tie(current_end, other_end) = edge_map[e];
      if(g.id(left_end) < g.id(dest_node)) swap(current_end, other_end);
      size_t other_unitig_id = get<0>(node_id_to_unitig_id[g.id(dest_node)]);
      if(this_unitig_id <= other_unitig_id && this_junction_end == current_end) {
        bool other_is_left_end = get<1>(node_id_to_unitig_id[g.id(dest_node)]);
        bool other_is_right_end = get<2>(node_id_to_unitig_id[g.id(dest_node)]);
        if(other_is_left_end && other_is_right_end) {
          // The other unitig is composed only by one node
          if(other_end == 'R') {
            gfa_edge new_edge(other_unitig_id, this_unitig_id, true, true);
            gfa_edges.push_back(new_edge);
          } else {
            gfa_edge new_edge(other_unitig_id, this_unitig_id, false, true);
            gfa_edges.push_back(new_edge);
          }
        } else {
          // The other node can be either the right end or the left end
          if(other_is_right_end) {
            gfa_edge new_edge(other_unitig_id, this_unitig_id, true, true);
            gfa_edges.push_back(new_edge);
          } else { // The other node is the left end
            gfa_edge new_edge(this_unitig_id, other_unitig_id, false, true);
            gfa_edges.push_back(new_edge);
          }
        }
      } // else do not consider the edge
    }

    ListGraph::Node right_end = g.nodeFromId(contracted_paths[unitig_id].back().first);
    this_unitig_id = get<0>(node_id_to_unitig_id[g.id(right_end)]);
    this_is_reversed = get<3>(node_id_to_unitig_id[g.id(right_end)]);
    this_junction_end = this_is_reversed ? 'L' : 'R'; // Consider only edges attached to this end
    for(ListGraph::IncEdgeIt e(g, right_end); e != INVALID; ++e ) {
      ListGraph::Node dest_node = g.u(e);
      if(g.id(dest_node) == g.id(right_end)) dest_node = g.v(e);
      char current_end, other_end;
      tie(current_end, other_end) = edge_map[e];
      if(g.id(right_end) <  g.id(dest_node)) swap(current_end, other_end);
      size_t other_unitig_id = get<0>(node_id_to_unitig_id[g.id(dest_node)]);
      if(this_unitig_id < other_unitig_id && this_junction_end == current_end) {
        bool other_is_left_end = get<1>(node_id_to_unitig_id[g.id(dest_node)]);
        bool other_is_right_end = get<2>(node_id_to_unitig_id[g.id(dest_node)]);
        if(other_is_left_end && other_is_right_end) {
          // The other unitig is composed only by one node
          if(other_end == 'L') {
            gfa_edge new_edge(this_unitig_id, other_unitig_id, true, true);
            gfa_edges.push_back(new_edge);
          } else {
            gfa_edge new_edge(this_unitig_id, other_unitig_id, true, false);
            gfa_edges.push_back(new_edge);
          }
        } else {
          // The other node can be either the right end or the left end
          if(other_is_left_end) {
            gfa_edge new_edge(this_unitig_id, other_unitig_id, true, true);
            gfa_edges.push_back(new_edge);
          } else { // The other node is the right end
            gfa_edge new_edge(this_unitig_id, other_unitig_id, true, false);
            gfa_edges.push_back(new_edge);
          }
        }
      }
    }
    string unitig_seq;
    size_t kc = 0;
    float km = 0.0f;
    for(const auto & x : contracted_paths[unitig_id]) {
      string seq = sequences_map[g.nodeFromId(x.first)];
      kc += kc_map[g.nodeFromId(x.first)];
      km = (km_map[g.nodeFromId(x.first)] * seq.size() + km * unitig_seq.size()) / (seq.size() + unitig_seq.size());
      if(x.second)
        rc_seq(seq);
      unitig_seq.append(seq.substr(0, seq.size() - min_overlap));
    }
    string seq = sequences_map[g.nodeFromId(contracted_paths[unitig_id].back().first)];
    if(contracted_paths[unitig_id].back().second)
      rc_seq(seq);
    unitig_seq.append(seq.substr(seq.size() - min_overlap, seq.size()));
    // if(2 * min_overlap < unitig_seq.size())
    //   km = km / (unitig_seq.size() - 2 * min_overlap);
    // if(km == 0)
    //   km = 1;
    cout << 'S' << '\t' << unitig_id << '\t' << unitig_seq << "\tLN:i:" << unitig_seq.size() << "\tKC:i:" << kc << "\tkm:f:" << km << '\n';
  }

  for(const auto & x : gfa_edges) {
    char source_rc = x.source_end ? '+' : '-';
    char target_rc = x.target_end ? '+' : '-';
    cout << 'L' << '\t' << x.source_id << '\t' << source_rc << '\t' << x.target_id << '\t' << target_rc << '\t' << min_overlap << 'M' << endl;
    if(double_link)
      cout << 'L' << '\t' << x.target_id << '\t' << complement_side(target_rc) << '\t' << x.source_id << '\t' << complement_side(source_rc) << '\t' << min_overlap << 'M' << endl;
  }
}

vector<pair<ListGraph::Node, char> > get_simple_path_from(const ListGraph &g, const ListGraph::Node &n,
                                                          const ListGraph::EdgeMap<pair<char, char> > &edge_map,
                                                          const char junction, unordered_set<int> &node_deleted) {
  vector<pair<ListGraph::Node, char> > r;
  if(node_deleted.find(g.id(n)) != node_deleted.end())
    return r;
  map<char, vector<pair<ListGraph::Node, char> > > neigh;
  neigh['R'] = vector<pair<ListGraph::Node, char> >();
  neigh['L'] = vector<pair<ListGraph::Node, char> >();
  for(ListGraph::IncEdgeIt e(g, n); e != INVALID; ++e) {
    auto dest_node = g.target(e);
    char current_node_end, other_node_end;
    tie(current_node_end, other_node_end) = edge_map[e];
    if(g.id(n) < g.id(dest_node)) swap(current_node_end, other_node_end);
    neigh[current_node_end].push_back(make_pair(dest_node, other_node_end));
  }
  if(neigh[junction].size() == 1) {
    node_deleted.insert(g.id(n));
    r.push_back(make_pair(n, junction));
    char free_end = junction == 'R' ? 'L' : 'R';
    if(neigh[free_end].size() == 1) {
      vector<pair<ListGraph::Node, char> > following = get_simple_path_from(g, neigh[free_end][0].first,
                                                                            edge_map, neigh[free_end][0].second,
                                                                            node_deleted);
      r.insert(r.end(), following.begin(), following.end());
    }
  }
  return r;
}
