# Compile
```
cd tir
make
```

Make sure that the the libraries listed under the External dependencies section are available in your system

# External dependencies
* [cmph - C Minimal Perfect Hashing Library](http://cmph.sourceforge.net/) (tested with version 2.0)
* zlib (tested with version 1.2.8)
* boost\_system and boost\_filesystem (tested with version 1.60)
* [lemon](http://lemon.cs.elte.hu/trac/lemon) (tested with version 1.3.1)

# Program Help
```
Usage: tir [OPTION] ...  GFAFILE
Removes tips from unitigs
OPTIONS
  -k, --k-size=K               size of kmers in dBG (required)
  -m, --max-k=M                maximum length of tip (required)
  -f, --fasta-file=FASTAFILE   fasta input file (optional)
  -C, --output-ccs             output ccs in directory ccs
  -D, --debug                  show debug messages

  -h, --help                   print this help and exit
  -v, --verbose                verbose output
```
