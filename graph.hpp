#ifndef TIR_GRAPH_HPP
#define TIR_GRAPH_HPP

#include <iostream>
#include <algorithm>
#include <bitset>
#include <cassert>
#include <vector>
#include <array>
#include <tuple>
#include <atomic>
#include <cstdint>

typedef uint16_t tiplen_t;

using namespace std;

enum nodetype {
  S = 0b00, // node label is "short"
  LNS = 0b01, // node is next to a "short"  node
  LNLNS = 0b10, // node is next to a "long" node that is next to a "short" node
  D = 0b11 // node is deleted
};

enum nodeend {
  LEFT = false,
  RIGHT = true
};

class Node {
  // Directed graph
  // the ID is a 32 bit integer
  array<uint32_t, 16> edges;
  // For each edge in position i, bits in position 2i and 2i+1 are set to the ends of the edges
  array<nodeend, 32> edges_prop;
  size_t KC;
  float KM;
  tiplen_t len;
  bool blockl, blockr;
  
public:
  static const uint32_t NO_NODE;

  Node();
  bool add_edge(const nodeend &thisend, const uint32_t &id, const nodeend &other_end);
  tuple<nodeend, uint32_t, nodeend> get_edge(const size_t &idx) const;
  size_t edge_num() const;
  void set_length(const tiplen_t &l);
  tiplen_t length() const;
  void set_kc(const size_t &kc);
  void set_km(const float &km);
  size_t get_kc() const;
  float get_km() const;
  vector<pair<uint32_t, nodeend> > left_edges() const;
  vector<pair<uint32_t, nodeend> > right_edges() const;
  pair<bool, nodeend> get_edge_to(const uint32_t &id, const nodeend &other_end);
  bool can_merge_left() const;
  bool can_merge_right() const;
  void block_left();
  void block_right();
};

class Graph {
  vector<Node> nodes;

public:
  Graph();
  Graph(const size_t &s);
  Node& get_node(const size_t &idx);
  Node& operator[](const size_t &idx);
  void resize(const size_t &s);
  size_t size() const;
  void clear();
};

vector<uint32_t> compute_cc(Graph &g);

nodeend nodeend_complement(const nodeend end);
bool end_to_bool(const nodeend &e);
nodeend bool_to_end(const bool &b);
char end_to_char(const nodeend &e);
nodeend char_to_end(const char &c);

#endif
