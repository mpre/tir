import numpy as np
import matplotlib.pyplot as plt
import sys

f = open(sys.argv[1])

vals = []
for x in f.readlines():
    k = int(x)
    vals.append(k)

maxsize = max(vals)

v = [[i, 0] for i in range(maxsize +1)]

for x in vals:
    v[x] = [v[x][0], v[x][1] + 1]

v = filter(lambda x : x[1] > 0, v)
c = [[v[x][0], v[x][0]*v[x][1]] for x in range(len(v))]

t_ = 0
cumul = []
for x in c:
    t_ += x[1]
    cumul.append(t_)

X = np.arange(len(v))
plt.bar(X, [x for x in cumul], align='center')
plt.xticks(X, [x[0] for x in c])
ymax  = max([x for x in cumul]) + 1

plt.ylim(0, ymax)

plt.show()
