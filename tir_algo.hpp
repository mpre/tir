#ifndef TIR_ALGO_HPP
#define TIR_ALGO_HPP

#include <vector>
#include <unordered_set>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <utility>
#include <map>
#include <zlib.h>

// #include "Snap.h"

#include "lemon/list_graph.h"
#include "lemon/lgf_writer.h"
#include "lemon/graph_to_eps.h"
#include "lemon/dim2.h"

#include "graph.hpp"
#include "util.hpp"

#include <cstdlib>
#include <ctime>

using namespace std;

// using namespace TSnap;
using namespace lemon;

// typedef TTriple<TStr, TBool, TBool > NodeTypeTIR;
// typedef TPair<TCh, TCh> EdgeTypeTIR;

// typedef TNodeEdgeNet<NodeTypeTIR, EdgeTypeTIR> GraphType;

struct smallnode {
  tiplen_t len;
  vector<pair<uint32_t, nodeend > > left_end_edges;
  vector<pair<uint32_t, nodeend> > right_end_edges;
  size_t kc;
  float km;
  bool block_left;
  bool block_right;

  smallnode();
  smallnode(const smallnode &other);
  void update_edge(const uint32_t &from_id, const uint32_t &to_id,
                   const bool flip);
};

struct smallgraph {
  map<uint32_t, smallnode> nodes;
};

bool clear_edges(smallgraph &cg, const uint32_t &id);

bool remove_from_list(vector<pair<uint32_t, nodeend> > &edges, const uint32_t &id,
                      const nodeend &other_end);

void remove_tips(const vector<uint32_t> &cc, Graph &g,
                 const unsigned int &kmin, const unsigned int &kmax);

bool tip_can_merge(smallgraph &cg, uint32_t &id);

pair<bool, nodeend> can_merge(const smallnode & node, const uint32_t target,
                              const nodeend &other_end);

vector<uint32_t> find_tips_of_length(const smallgraph &cd, const size_t &len);

vector<pair<uint32_t, nodeend> > remove_edges(smallgraph &cg,
                                              vector<pair<uint32_t, nodeend> > &removed_edges,
                                              const uint32_t &source_id);

void merge_nodes(smallgraph &cg, const uint32_t &firstid, const nodeend &firstend,
                 const uint32_t &secondnode, const nodeend &secondend,
                 const size_t &overlap, map<uint32_t, pair<bool, uint32_t> > &rename);

void output_single_arg(stringstream &o, const char &op, const uint32_t &id);

void output_remove(stringstream &o, const uint32_t &id);

void output_test(stringstream &o, const uint32_t &id);

void output_merge(stringstream &o, const uint32_t &firstid,
                  const char &firstend, const uint32_t &secondid, const char &secondend);

map<size_t, pair<bool, size_t> >
merge_and_remove(const vector<vector<uint32_t>> &ccs, const string &fastafilename,
                 const size_t &alive_num, const size_t &min_overlap);

tuple<uint32_t, uint32_t, nodeend, nodeend> find_mergeable_edge(smallgraph &cg);

// pair<bool, nodeend> test_if_single_edge(smallgraph &cg, const uint32_t &id1, const uint32_t &id2);

void rc_seq(string &seq);

char rc_char(char &c);

bool test_alive(const vector<char> &bv, const size_t &id);

void merge_l_to_l(map<size_t, pair<bool, size_t> > &rename, const size_t &min_overlap, const bool double_link);

vector<pair<ListGraph::Node, char> > get_simple_path_from(const ListGraph &g, const ListGraph::Node &n,
                                                          const ListGraph::EdgeMap<pair<char, char> > &edge_map,
                                                          const char junction, unordered_set<int> &node_deleted);

#endif
