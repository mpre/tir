#include "graph.hpp"

const uint32_t Node::NO_NODE = numeric_limits<uint32_t>::min();

Node::Node() : edges(), len(0), blockl(false), blockr(false) {
  edges.fill(NO_NODE);
};

bool Node::add_edge(const nodeend &this_end, const uint32_t &id,
                    const nodeend &other_end) {
  auto it = find(edges.begin(), edges.end(), NO_NODE);
  if(it == edges.end()) return false;
  *it = id;
  size_t index = (it - edges.begin())*2;
  edges_prop[index] = this_end;
  edges_prop[index + 1] =  other_end;
  return true;
}

tuple<nodeend, uint32_t, nodeend> Node::get_edge(const size_t &idx)
  const {
  return make_tuple(edges_prop[idx*2], edges[idx],
                    edges_prop[idx*2 + 1]);
}

size_t Node::edge_num() const {
  return find(edges.begin(), edges.end(), NO_NODE) - edges.begin();
}

void Node::set_length(const tiplen_t &l) {
  len = l;
}

tiplen_t Node::length() const {
  return len;
}

void Node::set_kc(const size_t &kc) {
  KC = kc;
}

void Node::set_km(const float &km) {
  KM = km;
}

size_t Node::get_kc() const {
  return KC;
}

float Node::get_km() const {
  return KM;
}

vector<pair<uint32_t, nodeend> > Node::left_edges() const {
  vector<pair<uint32_t, nodeend> > r;
  for(size_t i = 0; i < edge_num(); ++i) {
    auto e = get_edge(i);
    if(get<0>(e) == nodeend::LEFT) {
      r.push_back(make_pair(get<1>(e), get<2>(e)));
    }
  }
  return r;
}

vector<pair<uint32_t, nodeend> > Node::right_edges() const {
  vector<pair<uint32_t, nodeend> > r;
  for(size_t i = 0; i < edge_num(); ++i) {
    auto e = get_edge(i);
    if(get<0>(e) == nodeend::RIGHT) {
      r.push_back(make_pair(get<1>(e), get<2>(e)));
    }
  }
  return r;
}

pair<bool, nodeend> Node::get_edge_to(const uint32_t &id, const nodeend &other_end) {
  for(size_t i = 0; i < 16; ++i) {
    if(edges[i] == id && edges_prop[i*2 + 1] == other_end) {
      nodeend end = edges_prop[i*2];
      return make_pair(true, end);
    }
  }
  return make_pair(false, nodeend::LEFT);
}

bool Node::can_merge_left() const {
  return !blockl;
}

bool Node::can_merge_right() const {
  return !blockr;
}

void Node::block_left() {
  blockl = true;
}

void Node::block_right() {
  blockr = true;
}


Graph::Graph() {
}

Graph::Graph(const size_t &s) : nodes(s) {
}

Node& Graph::get_node(const size_t &idx) {
  assert(idx < nodes.size());
  return nodes[idx];
}

Node& Graph::operator[](const size_t &idx) {
  return nodes[idx];
}

void Graph::resize(const size_t &s) {
  nodes.resize(s+1);
}

size_t Graph::size() const {
  return nodes.size();
}

void Graph::clear() {
  nodes.clear();
}

vector<uint32_t> compute_cc(Graph &g) {
  vector<uint32_t> colors(g.size());
  //#pragma omp parallel for shared(colors, g)
  for(size_t i = 0; i < g.size(); ++i)
    colors[i] = i;
  bool change = true;
  while(change) {
    change = false;
    //#pragma omp parallel for shared(change, g, colors)
    for(uint32_t u = 0; u < g.size(); ++u) {
      for(size_t e = 0; e < g[u].edge_num(); ++e) {
        uint32_t v = get<1>(g[u].get_edge(e));
        if(colors[u] < colors[v] && colors[v] == colors[colors[v]]) {
            change = true;
            colors[colors[v]] = colors[u];
        }
      }
    }
    //#pragma omp parallel for shared(colors, g)
    for(uint32_t i = 0; i < g.size(); ++i) {
      while(colors[i] != colors[colors[i]])
        colors[i] = colors[colors[i]];
    }
  }
  return colors;
}

nodeend nodeend_complement(const nodeend end) {
  if(end == nodeend::RIGHT) return nodeend::LEFT;
  return nodeend::RIGHT;
}

nodeend bool_to_end(const bool &b) {
  if(b) return nodeend::RIGHT;
  return nodeend::LEFT;
}

bool end_to_bool(const nodeend &e) {
  // nodeend::RIGHT is set to TRUE
  return e == nodeend::RIGHT;
}

nodeend char_to_end(const char &c) {
  if(c == 'R') return nodeend::RIGHT;
  return nodeend::LEFT;
}

char end_to_char(const nodeend &e) {
  if(e == nodeend::RIGHT) return 'R';
  else return 'L';
}
